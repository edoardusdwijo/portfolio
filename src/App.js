import './App.css';
import React from 'react';
import Navigationbar from './components/Navigationbar';
import Intro from './components/Intro';
import Project from './components/Project';
import About from './components/About';
import Contact from './components/Contact';

function App() {
  return (
    <>
      {/* Start Navbar */}
      <div className="navigation">
        <Navigationbar />
      </div>
      {/* End Navbar */}

      {/* Start Intro */}
      <div className="introduction">
        <Intro />
      </div>
      {/* End Intro */}

      {/* Start Project */}
      <div className="project">
        <Project />
      </div>
      {/* End Project */}
      {/* Start About */}
      <div className="about">
        <About />
      </div>
      {/* End About */}
      {/* Start Contact */}
      <div className="contact">
        <Contact />
      </div>
      {/* End Contact */}
    </>
  );
}

export default App;
