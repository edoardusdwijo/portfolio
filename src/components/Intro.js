import { Col, Row } from 'react-bootstrap';
import React, { useState, useEffect } from 'react';
import axios from 'axios';

const Intro = () => {
  const [home, setHome] = useState([]);

  useEffect(() => {
    getHome();
  }, []);

  const getHome = async () => {
    const response = await axios.get('http://192.168.1.21:5000/home');
    setHome(response.data);
  };

  return (
    <div className="intro d-flex justify-content-center align-items-center" id="home">
      <div className="container d-flex justify-content-center align-items-center text-center">
        {home.map((home, index) => (
          <Col key={home.id}>
            <Row>
              <p className="Perkenalan">
                Hallo, Saya <span>{home.nama}</span> saya adalah seorang
              </p>
            </Row>
            <Row>
              <h1 className="bidang">{home.minat}</h1>
            </Row>
            <Row>
              <p className="deskpripsi">{home.deskripsi}</p>
            </Row>
            <Row>
              <a href="/path/to/cv.pdf" download className="btn btn-primary btn-download">
                Download CV
              </a>
            </Row>
          </Col>
        ))}
      </div>
    </div>
  );
};

export default Intro;
