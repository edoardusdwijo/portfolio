import { Navbar, Container, Nav } from 'react-bootstrap';
import { useState, useEffect } from 'react';

const Navigationbar = () => {
  const [activeNavLink, setActiveNavLink] = useState('home');

  useEffect(() => {
    // Set activeNavLink to 'home' when the component mounts
    setActiveNavLink('home');
  }, []);

  return (
    <Navbar collapseOnSelect expand="md" className="navbar-custom" fixed="top">
      <Container>
        <Navbar.Brand className="navbar-brand">Edoardus Dwijo Wijayanto</Navbar.Brand>
        <Navbar.Toggle aria-controls="responsive-navbar-nav" />
        <Navbar.Collapse id="responsive-navbar-nav">
          <Nav className="navbar-links ms-auto">
            <Nav.Item>
              <Nav.Link href="#home" className={activeNavLink === 'home' ? 'nav-link active' : 'nav-link'} onClick={() => setActiveNavLink('home')}>
                Home
              </Nav.Link>
            </Nav.Item>
            <Nav.Item>
              <Nav.Link href="#project" className={activeNavLink === 'project' ? 'nav-link active' : 'nav-link'} onClick={() => setActiveNavLink('project')}>
                Project
              </Nav.Link>
            </Nav.Item>
            <Nav.Item>
              <Nav.Link href="#about" className={activeNavLink === 'about' ? 'nav-link active' : 'nav-link'} onClick={() => setActiveNavLink('about')}>
                About
              </Nav.Link>
            </Nav.Item>
            <Nav.Item>
              <Nav.Link href="#contact" className={activeNavLink === 'contact' ? 'nav-link active' : 'nav-link'} onClick={() => setActiveNavLink('contact')}>
                Contact
              </Nav.Link>
            </Nav.Item>
          </Nav>
        </Navbar.Collapse>
      </Container>
    </Navbar>
  );
};

export default Navigationbar;
