import { Col, Row, Card, Button } from 'react-bootstrap';
import React, { useState } from 'react';

const Project = () => {
  const projects = [
    {
      id: 1,
      title: 'Pembuatan Website Pengaduan Masyarakat Tanjung',
      description:
        'Website Sistem Informasi Penyampaian Aspirasi guna untuk mempermudah masyarakat dalam menyampaikan aspirasi dan melihat informasi seputar kelurahan tanjung, dan untuk mempermudah pihak kelurahan Tanjung dalam mendata, menjawab, dan menangani aspirasi dari masyarakat kelurahan Tanjung.',
      image: require('../img/1.png'),
    },
    {
      id: 2,
      title: 'Pembuatan Website Puskesmas',
      description:
        'Merancang antarmuka atau tampilan website untuk Puskesmas di wilayah Jatilawang sesuai dengan desain yang telah disediakan oleh pelanggan, dengan mengimplementasikan bahasa pemrograman PHP, CSS, dan memanfaatkan framework Bootstrap 5 untuk memastikan keberlanjutan dan kemudahan pemeliharaan.',
      image: require('../img/5.png'),
    },
    {
      id: 3,
      title: 'Pembuatan Website Sell Maintenance',
      description:
        'Website Sell Maintenance adalah platform digital yang dibuat untuk melakukan pencatatan dan pengarsipan aktivitas pemeliharaan serta perawatan pada wesel dan lengkung. Proyek pembangunan website ini merupakan hasil kolaborasi yang dilakukan selama masa Praktek Kerja Lapangan (PKL) di PT.KAI DAOP 5 Purwokerto, yang melibatkan sebuah tim yang terdiri dari lima orang.',
      image: require('../img/2.png'),
    },
    {
      id: 4,
      title: 'Pembuatan Website Trivia D-Keb',
      description:
        'Merancang antarmuka atau tampilan website untuk Dasa Wisma di kawasan Desa Kebumen sesuai dengan desain yang telah disampaikan oleh pelanggan. Proyek pengembangan ini menggunakan bahasa pemrograman PHP, CSS, dan menggunakan framework Bootstrap 5 untuk menciptakan pengalaman pengguna yang optimal.',
      image: require('../img/3.png'),
    },
    {
      id: 5,
      title: 'Pembuatan Website untuk Serifikasi',
      description:
        'Pengembangan situs web untuk pelaksanaan uji sertifikasi yang diselenggarakan oleh Badan Nasional Sertifikasi Profesi (BNSP). Untuk membuat website yang sesuai dengan ketentuan, saya memilih menggunakan framework Laravel dan mengintegrasikan elemen-elemen desain yang modern melalui Bootstrap 5.',
      image: require('../img/4.png'),
    },
  ];

  const [visibleProjects, setVisibleProjects] = useState(3);
  const [showMore, setShowMore] = useState(false);

  const showMoreProjects = () => {
    setVisibleProjects(visibleProjects + 3);
    setShowMore(true);
  };

  return (
    <>
      <div className="judul container text-center mb-5" id="project">
        <h2 className="pt-4">Project</h2>
      </div>

      <div className={`cardProject container mb-5 ${showMore ? 'show-more' : ''}`}>
        <Row xs={1} md={3} className="g-4">
          {projects.slice(0, visibleProjects).map((project) => (
            <Col key={project.id}>
              <Card className="shadow">
                <Card.Img variant="top" src={project.image} loading="lazy" />
                <Card.Body>
                  <Card.Title className="text-center">{project.title}</Card.Title>
                  <Card.Text>{project.description}</Card.Text>
                </Card.Body>
              </Card>
            </Col>
          ))}
        </Row>
        {visibleProjects < projects.length && (
          <div className="text-center mt-4 showmore">
            <Button variant="primary" onClick={showMoreProjects}>
              Lebih banyak
            </Button>
          </div>
        )}
      </div>
    </>
  );
};

export default Project;
