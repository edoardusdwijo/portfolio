import { Container } from 'react-bootstrap';
import React, { useState, useEffect } from 'react';
import axios from 'axios';

const About = () => {
  const [about, setAbout] = useState([]);

  useEffect(() => {
    getAbout();
  }, []);

  const getAbout = async () => {
    const response = await axios.get('http://192.168.1.21:5000/about');
    setAbout(response.data);
  };
  return (
    <>
      <section className="about-section" id="about">
        {about.map((about, index) => (
          <Container key={about.id}>
            <h2 className="section-title text-center pt-4">About Me</h2>
            <div className="about-content">
              <div className="about-text">
                <h3 className="about-subtitle">{about.nama}</h3>
                <p>{about.deskripsi}</p>
              </div>
              <div className="about-image">
                <img src={require('../img/7.jpg')} alt="About Me" loading="lazy" />
              </div>
            </div>
          </Container>
        ))}
      </section>
    </>
  );
};

export default About;
