FROM node:16-alpine

WORKDIR /app

COPY package* .
RUN npm i
RUN npm i axios

COPY . .

CMD ["npm", "start"]